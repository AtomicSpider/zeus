﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using ImageProcessor;
using ImageProcessor.Imaging.Formats;
using ImageProcessor.Imaging;

namespace Zeus
{
    public partial class MainForm : Form
    {
        //Final
        readonly string formats = "All Image Files | *.png; *.jpg; *.jpeg";
        readonly string outputExtension = ".png";
        readonly ISupportedImageFormat imageFactoryFormat = new PngFormat { Quality = 100 };

        //Flags
        bool fileOpened = false;
        bool heightConstrain = true;

        //Variables
        string mode = null;
        int mScale = 0;
        string initialDir = "";
        int source_padding = 0;
        int source_opacity = 0;

        //Data
        ImageFactory imageFactoryM = new ImageFactory(preserveExifData: true);
        ImageFactory imageFactoryH = new ImageFactory(preserveExifData: true);
        ImageFactory imageFactoryX = new ImageFactory(preserveExifData: true);
        ImageFactory imageFactoryXX = new ImageFactory(preserveExifData: true);
        ImageFactory imageFactoryXXX = new ImageFactory(preserveExifData: true);

        public MainForm()
        {
            InitializeComponent();
        }

        /** MAIN BUTTON CLICKS *****************************************************/

        private void button_launcher_icons_Click(object sender, EventArgs e)
        {
            //ToDo reset launcher form
            mode = "launcher";
            onPanelClick();
        }

        private void button_home_Click(object sender, EventArgs e)
        {
            mode = "home";
            resetViews();
            onPanelClick();
        }

        /** STATE CHANGE *****************************************************/

        private void resetViews()
        {
            //Reset Variables
            fileOpened = false;
            heightConstrain = true;
            initialDir = "";
            source_padding = 0;
            source_opacity = 0;

            //Reset Views
            setSourceText("");
            setDestinationText("");

            picture_box_main.Image = null;

            trackbar_source_padding.Value = 0;
            trackbar_source_opacity.Value = 0;

            picture_box_xxx.Image = null;
            picture_box_xx.Image = null;
            picture_box_x.Image = null;
            picture_box_h.Image = null;
            picture_box_m.Image = null;

            resetStatus();

            disableSubControls();
        }

        private void disableSubControls()
        {
            trackbar_source_padding.Enabled = false;
            trackbar_source_opacity.Enabled = false;
            button_source_color_picker.Enabled = false;
            button_source_color_picker.BackColor = Color.FromArgb(223, 87, 69);
        }

        private void enableSubControls()
        {
            trackbar_source_padding.Enabled = true;
            trackbar_source_opacity.Enabled = true;
            button_source_color_picker.Enabled = true;
        }

        private void onPanelClick()
        {
            switch (mode)
            {
                case "launcher":
                    mScale = 48;
                    panel_submain.Visible = true;
                    panel_size_padding.Visible = false;
                    panel_output_name.Visible = false;
                    break;
                case "home":
                    panel_main.Visible = true;
                    panel_submain.Visible = false;
                    break;
            }
        }

        /** BUTTON CLICKS *****************************************************/

        private void button_source_Click(object sender, EventArgs e)
        {
            //clear status
            resetStatus();

            //Open Dialog
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select Image";
            openFileDialog.Filter = formats;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                //Reset Controls
                resetViews();

                //Fill the dir boxes
                string filename = openFileDialog.FileName;
                setSourceText(filename);
                setDestinationText(Path.GetDirectoryName(filename) + "\\" + Path.GetFileNameWithoutExtension(filename) + "_Zeus\\");

                //Set vars
                fileOpened = true;
                initialDir = Path.GetDirectoryName(filename);
                using (Image image = Image.FromFile(filename))
                {
                    if (image.Width > image.Height) heightConstrain = false;
                }

                //Set Image Picturebox
                picture_box_main.ImageLocation = filename;

                //Preview Output Images
                previewImages();

                //Enable Sub Controls
                enableSubControls();
            }
        }

        private void previewImages()
        {
            byte[] photoBytes = null;
            //Generate Overlay
            if (source_opacity != 0)
            {
                Bitmap mBitmap = (Bitmap)Image.FromFile(getSourceText());
                mBitmap = getOverlayBitmap(mBitmap);
                photoBytes = BitmapToByte(mBitmap);
            }
            //Using Lib
            else
            {
                photoBytes = File.ReadAllBytes(getSourceText());
            }

            int scale = 0;
            int resolution = 0;

            using (MemoryStream inStream = new MemoryStream(photoBytes))
            {
                for (int i = 0; i < 5; i++)
                {
                    switch (i)
                    {
                        case 0:
                            scale = mScale;
                            break;
                        case 1:
                            scale = (int)(mScale * 1.5);
                            break;
                        case 2:
                            scale = (int)(mScale * 2);
                            break;
                        case 3:
                            scale = (int)(mScale * 3);
                            break;
                        case 4:
                            scale = (int)(mScale * 4);
                            break;
                    }

                    resolution = scale;
                    //Subcontrols
                    scale += (int)((scale * source_padding) / 100.0);

                    //Size
                    Size size;
                    if (heightConstrain) size = new Size(0, scale);
                    else size = new Size(scale, 0);

                    using (MemoryStream outStream = new MemoryStream())
                    {
                        // Initialize the ImageFactory using the overload to preserve EXIF metadata.
                        using (ImageFactory imageFactory = new ImageFactory(preserveExifData: true))
                        {
                            // Load, resize, set the format and quality and save an image.
                            imageFactory.Load(inStream)
                                        .Resize(size);

                            if (source_padding > 0)
                            {
                                int imgH = imageFactory.Image.Height;
                                int imgW = imageFactory.Image.Width;
                                int excessH = imgH - resolution;
                                int excessW = imgW - resolution;
                                int excessT = 0, excessB = imgH, excessL = 0, excessR = imgW;

                                if (excessH > 0)
                                {
                                    excessT = excessH / 2;
                                    excessB = resolution;
                                }
                                if (excessW > 0)
                                {
                                    excessL = excessW / 2;
                                    excessR = resolution;
                                }

                                imageFactory.Crop(new CropLayer(excessL, excessT, excessR, excessB, CropMode.Pixels));
                            }

                            imageFactory.Resize(new ResizeLayer((new Size(resolution, resolution)), ResizeMode.BoxPad))
                                        .Format(imageFactoryFormat)
                                        .Save(outStream);
                        }

                        // Do something with the stream.
                        switch (i)
                        {
                            case 0:
                                imageFactoryM.Load(outStream);
                                picture_box_m.Image = Image.FromStream(outStream);
                                break;
                            case 1:
                                imageFactoryH.Load(outStream);
                                picture_box_h.Image = Image.FromStream(outStream);
                                break;
                            case 2:
                                imageFactoryX.Load(outStream);
                                picture_box_x.Image = Image.FromStream(outStream);
                                break;
                            case 3:
                                imageFactoryXX.Load(outStream);
                                picture_box_xx.Image = Image.FromStream(outStream);
                                break;
                            case 4:
                                imageFactoryXXX.Load(outStream);
                                picture_box_xxx.Image = Image.FromStream(outStream);
                                break;
                        }
                    }
                }
            }
        }

        private Bitmap getOverlayBitmap(Bitmap bitmap)
        {
            int alpha = (int)((255 * source_opacity) / 100.0);
            Color chosenColor = button_source_color_picker.BackColor;
            Color overlayColor = Color.FromArgb(alpha, chosenColor.R, chosenColor.G, chosenColor.B);
            Color actualColor;

            // Make an empty bitmap the same size as scrBitmap
            Bitmap newBitmap = new Bitmap(bitmap.Width, bitmap.Height);
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    // Get the pixel from the scrBitmap image
                    actualColor = bitmap.GetPixel(i, j);
                    // If not completely transparent
                    if (actualColor.A != 0)
                    {
                        newBitmap.SetPixel(i, j, overlayColor);
                    }
                    else
                        newBitmap.SetPixel(i, j, actualColor);
                }
            }

            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                graphics.DrawImage(newBitmap, new Rectangle(0, 0, newBitmap.Width, newBitmap.Height));
            }
            return bitmap;
        }

        public static byte[] BitmapToByte(Bitmap bitmap)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(bitmap, typeof(byte[]));
        }

        private void button_destination_Click(object sender, EventArgs e)
        {
            //clear status
            resetStatus();

            //Open Dialog
            FolderBrowserDialog chooseFolderDialog = new FolderBrowserDialog();
            chooseFolderDialog.Description = "Choose Destination Folder";
            chooseFolderDialog.ShowNewFolderButton = true;
            if (fileOpened) chooseFolderDialog.SelectedPath = initialDir;

            if (chooseFolderDialog.ShowDialog() == DialogResult.OK)
            {
                //Fill the dir box
                setDestinationText(chooseFolderDialog.SelectedPath + "\\");
            }
        }

        private void button_color_picker_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();

            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                //Change Button Color
                int color = colorDialog.Color.ToArgb();
                button_source_color_picker.BackColor = Color.FromArgb(color);

                previewImages();
            }
        }

        private void button_generate_Click(object sender, EventArgs e)
        {
            //clear status
            resetStatus();

            //Validate info
            switch (mode)
            {
                case "launcher":
                    if (getDestinationText().Equals("") || getSourceText().Equals(""))
                    {
                        showErrorStatus("Please choose a valid image file");
                        return;
                    }
                    break;
            }

            //File Strings
            string subdir, outFileName, subdirXXX, subdirXX, subdirX, subdirH, subdirM;

            if (mode.Equals("launcher"))
            {
                subdir = "mipmap-";
                outFileName = "ic_launcher" + outputExtension;
            }
            else
            {
                subdir = "drawable-";
                outFileName = "ic_icon" + outputExtension;
            }
            subdirXXX = getDestinationText() + "res\\" + subdir + "xxxhdpi\\";
            subdirXX = getDestinationText() + "res\\" + subdir + "xxhdpi\\";
            subdirX = getDestinationText() + "res\\" + subdir + "xhdpi\\";
            subdirH = getDestinationText() + "res\\" + subdir + "hdpi\\";
            subdirM = getDestinationText() + "res\\" + subdir + "mdpi\\";

            //Create Main Dir
            Directory.CreateDirectory(getDestinationText() + "res\\");
            //Create Sub Dir
            Directory.CreateDirectory(subdirXXX);
            Directory.CreateDirectory(subdirXX);
            Directory.CreateDirectory(subdirX);
            Directory.CreateDirectory(subdirH);
            Directory.CreateDirectory(subdirM);

            //Save Files
            for (int i = 0; i < 5; i++)
            {
                switch (i)
                {
                    case 0:
                        imageFactoryM.Save(subdirM + outFileName);
                        break;
                    case 1:
                        imageFactoryH.Save(subdirH + outFileName);
                        break;
                    case 2:
                        imageFactoryX.Save(subdirX + outFileName);
                        break;
                    case 3:
                        imageFactoryXX.Save(subdirXX + outFileName);
                        break;
                    case 4:
                        imageFactoryXXX.Save(subdirXXX + outFileName);
                        break;
                }
            }

            MessageBox.Show("Done");
        }

        /** HELPER METHODS *****************************************************/

        private void resetStatus()
        {
            text_box_status.Text = "";
        }

        private void showErrorStatus(string status)
        {
            text_box_status.ForeColor = Color.FromArgb(223, 87, 69);
            text_box_status.Text = status;
        }

        private void showNormalStatus(string status)
        {
            text_box_status.ForeColor = Color.FromArgb(170, 170, 170);
            text_box_status.Text = status;
        }

        /** GETTERS AND SETTERS *****************************************************/

        private void setSourceText(string sourceText)
        {
            text_box_source.Text = sourceText;
        }

        private void setDestinationText(string destinationText)
        {
            text_box_destination.Text = destinationText;
        }

        private string getSourceText()
        {
            return text_box_source.Text;
        }

        private string getDestinationText()
        {
            return text_box_destination.Text;
        }

        /** TEXT FIELD CHANGES *****************************************************/

        private void text_box_source_TextChanged(object sender, EventArgs e)
        {

        }

        private void text_box_destination_TextChanged(object sender, EventArgs e)
        {

        }

        /** SCROLL CHANGES *****************************************************/

        private void trackbar_source_padding_MouseUp(object sender, MouseEventArgs e)
        {
            if (source_padding != trackbar_source_padding.Value)
            {
                source_padding = trackbar_source_padding.Value;
                previewImages();
            }
        }

        private void trackbar_source_opacity_MouseUp(object sender, MouseEventArgs e)
        {
            if (source_opacity != trackbar_source_opacity.Value)
            {
                source_opacity = trackbar_source_opacity.Value;
                previewImages();
            }
        }

        private void trackbar_source_padding_ValueChanged(object sender, EventArgs e)
        {
            label_source_padding_value.Text = "" + trackbar_source_padding.Value + "%";
        }

        private void trackbar_source_opacity_ValueChanged(object sender, EventArgs e)
        {
            label_source_opacity_value.Text = "" + trackbar_source_opacity.Value + "%";
        }

        //Disable key events
        private void trackbar_source_padding_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }
        //Disable mouse wheel
        private void trackbar_source_padding_MouseWheel(object sender, MouseEventArgs e)
        {
            ((HandledMouseEventArgs)e).Handled = true;
        }

        //Disable key events
        private void trackbar_source_opacity_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }
        //Disable mouse wheel
        private void trackbar_source_opacity_MouseWheel(object sender, MouseEventArgs e)
        {
            ((HandledMouseEventArgs)e).Handled = true;
        }
    }
}
