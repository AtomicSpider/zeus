﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace Zeus
{
    public partial class ApplicationForm : Form
    {
        string formats = "All Image Files | *.png; *.jpg; *.jpeg";

        public ApplicationForm()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        /** BUTTON CLICKS *****************************************************/

        private void sourceButton_Click(object sender, EventArgs e)
        {
            //clear status
            resetStatus();

            //Open Dialog
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select Image";
            openFileDialog.Filter = formats;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                //Fill the dir boxes
                string filename = openFileDialog.FileName;
                setSourceText(filename);
                setDestinationText(Path.GetDirectoryName(filename) + "\\" + Path.GetFileNameWithoutExtension(filename) + "_trimmed" + Path.GetExtension(filename));

                //Set vars for folder
                //fileOpened = true;
                //initialDir = Path.GetDirectoryName(filename);
            }
        }

        private void destinationButton_Click(object sender, EventArgs e)
        {
            //clear status
            resetStatus();

            //Check src file validity
            string tempSrcFileName = Path.GetFileName(getSourceText());
            if (tempSrcFileName != null && tempSrcFileName != "")
            {
                //Save Dialog
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Title = "Select output as";
                saveFileDialog.Filter = formats;

                string tempDestFileName = Path.GetFileName(getDestinationText());
                if (tempDestFileName != null && tempDestFileName != "") saveFileDialog.FileName = tempDestFileName;

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    setDestinationText(saveFileDialog.FileName);
            }
            else
                showErrorStatus("Please select a valid source file");
        }

        private void trimButton_Click(object sender, EventArgs e)
        {
            //clear status
            resetStatus();


        }

        /** HELPER METHODS *****************************************************/

        private string copyVideoCommand(TimeSpan from, TimeSpan interval, string input, string output)
        {
            return
                "-ss" + " " +
                from + " " +
                //Input file
                "-i" + " \"" +
                input + "\" " +
                //Output args
                "-t" + " " +
                interval + " " +
                "-c" + " " +
                "copy" + " " +
                //
                "-map 0" + " " +
                "-y" + " " +
                "\"" + output + "\"";
        }

        private void resetStatus()
        {
            statusTextBox.Text = "";

        }

        private void showErrorStatus(string status)
        {
            statusTextBox.ForeColor = Color.FromArgb(223, 87, 69);
            statusTextBox.Text = status;
        }

        private void showNormalStatus(string status)
        {
            statusTextBox.ForeColor = Color.FromArgb(170, 170, 170);
            statusTextBox.Text = status;
        }

        /** GETTERS AND SETTERS *****************************************************/

        private void setSourceText(string sourceText)
        {
            sourceTextBox.Text = sourceText;
        }

        private void setDestinationText(string destinationText)
        {
            destinationTextBox.Text = destinationText;
        }

        private string getSourceText()
        {
            return sourceTextBox.Text;
        }

        private string getDestinationText()
        {
            return destinationTextBox.Text;
        }

        /** TEXT FIELD CHANGED *****************************************************/

        private void sourceTextBox_TextChanged(object sender, EventArgs e)
        {
            resetStatus();
        }

        private void destinationTextBox_TextChanged(object sender, EventArgs e)
        {
            resetStatus();
        }

        private void fromTextBox_TextChanged(object sender, EventArgs e)
        {
            resetStatus();
        }

        private void toTextBox_TextChanged(object sender, EventArgs e)
        {
            resetStatus();
        }

        /** CMD LOG *****************************************************/

        /*static void cmd_DataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine("Output from other process");
            Console.WriteLine(e.Data);
        }

        static void cmd_Error(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine("Error from other process");
            Console.WriteLine(e.Data);
        }*/
    }
}
