﻿namespace Zeus
{
    partial class ApplicationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplicationForm));
            this.sourceButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.destinationButton = new System.Windows.Forms.Button();
            this.sourceTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.destinationTextBox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.statusTextBox = new System.Windows.Forms.Label();
            this.trimButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sourceButton
            // 
            this.sourceButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.sourceButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sourceButton.FlatAppearance.BorderSize = 0;
            this.sourceButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.sourceButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.sourceButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sourceButton.ForeColor = System.Drawing.Color.White;
            this.sourceButton.Location = new System.Drawing.Point(445, 58);
            this.sourceButton.Name = "sourceButton";
            this.sourceButton.Size = new System.Drawing.Size(75, 23);
            this.sourceButton.TabIndex = 1;
            this.sourceButton.Text = "Browse";
            this.sourceButton.UseVisualStyleBackColor = false;
            this.sourceButton.Click += new System.EventHandler(this.sourceButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(37, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Source";
            // 
            // destinationButton
            // 
            this.destinationButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.destinationButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.destinationButton.FlatAppearance.BorderSize = 0;
            this.destinationButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.destinationButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.destinationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.destinationButton.ForeColor = System.Drawing.Color.White;
            this.destinationButton.Location = new System.Drawing.Point(445, 122);
            this.destinationButton.Name = "destinationButton";
            this.destinationButton.Size = new System.Drawing.Size(75, 23);
            this.destinationButton.TabIndex = 4;
            this.destinationButton.Text = "Browse";
            this.destinationButton.UseVisualStyleBackColor = false;
            this.destinationButton.Click += new System.EventHandler(this.destinationButton_Click);
            // 
            // sourceTextBox
            // 
            this.sourceTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(63)))));
            this.sourceTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sourceTextBox.ForeColor = System.Drawing.Color.White;
            this.sourceTextBox.Location = new System.Drawing.Point(39, 58);
            this.sourceTextBox.Name = "sourceTextBox";
            this.sourceTextBox.Size = new System.Drawing.Size(400, 20);
            this.sourceTextBox.TabIndex = 5;
            this.sourceTextBox.TextChanged += new System.EventHandler(this.sourceTextBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(37, 102);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Destination";
            // 
            // destinationTextBox
            // 
            this.destinationTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(63)))));
            this.destinationTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.destinationTextBox.ForeColor = System.Drawing.Color.White;
            this.destinationTextBox.Location = new System.Drawing.Point(39, 122);
            this.destinationTextBox.Name = "destinationTextBox";
            this.destinationTextBox.Size = new System.Drawing.Size(400, 20);
            this.destinationTextBox.TabIndex = 7;
            this.destinationTextBox.TextChanged += new System.EventHandler(this.destinationTextBox_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.panel1.Controls.Add(this.statusTextBox);
            this.panel1.Controls.Add(this.trimButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 252);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(868, 63);
            this.panel1.TabIndex = 0;
            // 
            // statusTextBox
            // 
            this.statusTextBox.AutoSize = true;
            this.statusTextBox.BackColor = System.Drawing.Color.Transparent;
            this.statusTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.statusTextBox.Location = new System.Drawing.Point(37, 14);
            this.statusTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.statusTextBox.MaximumSize = new System.Drawing.Size(374, 37);
            this.statusTextBox.MinimumSize = new System.Drawing.Size(374, 37);
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.Size = new System.Drawing.Size(374, 37);
            this.statusTextBox.TabIndex = 12;
            // 
            // trimButton
            // 
            this.trimButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.trimButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.trimButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.trimButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(73)))), ((int)(((byte)(74)))));
            this.trimButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.trimButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.trimButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.trimButton.ForeColor = System.Drawing.Color.White;
            this.trimButton.Location = new System.Drawing.Point(779, 14);
            this.trimButton.Name = "trimButton";
            this.trimButton.Size = new System.Drawing.Size(75, 37);
            this.trimButton.TabIndex = 12;
            this.trimButton.Text = "Trim";
            this.trimButton.UseVisualStyleBackColor = false;
            this.trimButton.Click += new System.EventHandler(this.trimButton_Click);
            // 
            // ApplicationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.ClientSize = new System.Drawing.Size(868, 315);
            this.Controls.Add(this.destinationTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.sourceTextBox);
            this.Controls.Add(this.destinationButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sourceButton);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ApplicationForm";
            this.Text = "Zeus";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button sourceButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button destinationButton;
        private System.Windows.Forms.TextBox sourceTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox destinationTextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button trimButton;
        private System.Windows.Forms.Label statusTextBox;
    }
}