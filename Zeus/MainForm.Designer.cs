﻿namespace Zeus
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel_main = new System.Windows.Forms.Panel();
            this.panel_submain = new System.Windows.Forms.Panel();
            this.panel_image_output_normal = new System.Windows.Forms.Panel();
            this.picture_box_xxx = new System.Windows.Forms.PictureBox();
            this.picture_box_xx = new System.Windows.Forms.PictureBox();
            this.picture_box_x = new System.Windows.Forms.PictureBox();
            this.picture_box_m = new System.Windows.Forms.PictureBox();
            this.picture_box_h = new System.Windows.Forms.PictureBox();
            this.panel_output_name = new System.Windows.Forms.Panel();
            this.label_output_name = new System.Windows.Forms.Label();
            this.text_box_output_name = new System.Windows.Forms.TextBox();
            this.panel_size_padding = new System.Windows.Forms.Panel();
            this.panel_padding = new System.Windows.Forms.Panel();
            this.label_dip_2 = new System.Windows.Forms.Label();
            this.text_box_padding = new System.Windows.Forms.TextBox();
            this.label_padding = new System.Windows.Forms.Label();
            this.panel_size = new System.Windows.Forms.Panel();
            this.label_dip_1 = new System.Windows.Forms.Label();
            this.text_box_size = new System.Windows.Forms.TextBox();
            this.label_size = new System.Windows.Forms.Label();
            this.panel_source_attr = new System.Windows.Forms.Panel();
            this.panel_source_color = new System.Windows.Forms.Panel();
            this.label_source_color_opacity = new System.Windows.Forms.Label();
            this.label_source_opacity_value = new System.Windows.Forms.Label();
            this.trackbar_source_opacity = new System.Windows.Forms.TrackBar();
            this.button_source_color_picker = new System.Windows.Forms.Button();
            this.label_source_color = new System.Windows.Forms.Label();
            this.panel_source_padding = new System.Windows.Forms.Panel();
            this.label_source_padding_value = new System.Windows.Forms.Label();
            this.trackbar_source_padding = new System.Windows.Forms.TrackBar();
            this.label_source_padding = new System.Windows.Forms.Label();
            this.panel_bottom = new System.Windows.Forms.Panel();
            this.text_box_status = new System.Windows.Forms.Label();
            this.button_home = new System.Windows.Forms.Button();
            this.button_generate = new System.Windows.Forms.Button();
            this.panel_dir = new System.Windows.Forms.Panel();
            this.label_source = new System.Windows.Forms.Label();
            this.button_source = new System.Windows.Forms.Button();
            this.button_destination = new System.Windows.Forms.Button();
            this.text_box_source = new System.Windows.Forms.TextBox();
            this.label_destination = new System.Windows.Forms.Label();
            this.text_box_destination = new System.Windows.Forms.TextBox();
            this.picture_box_main = new System.Windows.Forms.PictureBox();
            this.button_action_bar_and_tab_icons = new System.Windows.Forms.Button();
            this.button_launcher_icons = new System.Windows.Forms.Button();
            this.panel_main.SuspendLayout();
            this.panel_submain.SuspendLayout();
            this.panel_image_output_normal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_xxx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_xx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_x)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_m)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_h)).BeginInit();
            this.panel_output_name.SuspendLayout();
            this.panel_size_padding.SuspendLayout();
            this.panel_padding.SuspendLayout();
            this.panel_size.SuspendLayout();
            this.panel_source_attr.SuspendLayout();
            this.panel_source_color.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackbar_source_opacity)).BeginInit();
            this.panel_source_padding.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackbar_source_padding)).BeginInit();
            this.panel_bottom.SuspendLayout();
            this.panel_dir.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_main)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_main
            // 
            this.panel_main.AutoSize = true;
            this.panel_main.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.panel_main.Controls.Add(this.panel_submain);
            this.panel_main.Controls.Add(this.button_action_bar_and_tab_icons);
            this.panel_main.Controls.Add(this.button_launcher_icons);
            this.panel_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_main.Location = new System.Drawing.Point(0, 0);
            this.panel_main.Name = "panel_main";
            this.panel_main.Size = new System.Drawing.Size(896, 650);
            this.panel_main.TabIndex = 0;
            // 
            // panel_submain
            // 
            this.panel_submain.AutoSize = true;
            this.panel_submain.Controls.Add(this.panel_image_output_normal);
            this.panel_submain.Controls.Add(this.panel_output_name);
            this.panel_submain.Controls.Add(this.panel_size_padding);
            this.panel_submain.Controls.Add(this.panel_source_attr);
            this.panel_submain.Controls.Add(this.panel_bottom);
            this.panel_submain.Controls.Add(this.panel_dir);
            this.panel_submain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_submain.Location = new System.Drawing.Point(0, 0);
            this.panel_submain.Name = "panel_submain";
            this.panel_submain.Size = new System.Drawing.Size(896, 650);
            this.panel_submain.TabIndex = 0;
            this.panel_submain.Visible = false;
            // 
            // panel_image_output_normal
            // 
            this.panel_image_output_normal.Controls.Add(this.picture_box_xxx);
            this.panel_image_output_normal.Controls.Add(this.picture_box_xx);
            this.panel_image_output_normal.Controls.Add(this.picture_box_x);
            this.panel_image_output_normal.Controls.Add(this.picture_box_m);
            this.panel_image_output_normal.Controls.Add(this.picture_box_h);
            this.panel_image_output_normal.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_image_output_normal.Location = new System.Drawing.Point(0, 329);
            this.panel_image_output_normal.Name = "panel_image_output_normal";
            this.panel_image_output_normal.Size = new System.Drawing.Size(896, 216);
            this.panel_image_output_normal.TabIndex = 24;
            // 
            // picture_box_xxx
            // 
            this.picture_box_xxx.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(58)))));
            this.picture_box_xxx.Location = new System.Drawing.Point(36, 16);
            this.picture_box_xxx.Name = "picture_box_xxx";
            this.picture_box_xxx.Size = new System.Drawing.Size(192, 192);
            this.picture_box_xxx.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picture_box_xxx.TabIndex = 15;
            this.picture_box_xxx.TabStop = false;
            // 
            // picture_box_xx
            // 
            this.picture_box_xx.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(58)))));
            this.picture_box_xx.Location = new System.Drawing.Point(234, 16);
            this.picture_box_xx.Name = "picture_box_xx";
            this.picture_box_xx.Size = new System.Drawing.Size(144, 144);
            this.picture_box_xx.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picture_box_xx.TabIndex = 16;
            this.picture_box_xx.TabStop = false;
            // 
            // picture_box_x
            // 
            this.picture_box_x.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(58)))));
            this.picture_box_x.Location = new System.Drawing.Point(384, 16);
            this.picture_box_x.Name = "picture_box_x";
            this.picture_box_x.Size = new System.Drawing.Size(96, 96);
            this.picture_box_x.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picture_box_x.TabIndex = 17;
            this.picture_box_x.TabStop = false;
            // 
            // picture_box_m
            // 
            this.picture_box_m.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(58)))));
            this.picture_box_m.Location = new System.Drawing.Point(564, 16);
            this.picture_box_m.Name = "picture_box_m";
            this.picture_box_m.Size = new System.Drawing.Size(48, 48);
            this.picture_box_m.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picture_box_m.TabIndex = 19;
            this.picture_box_m.TabStop = false;
            // 
            // picture_box_h
            // 
            this.picture_box_h.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(58)))));
            this.picture_box_h.Location = new System.Drawing.Point(486, 16);
            this.picture_box_h.Name = "picture_box_h";
            this.picture_box_h.Size = new System.Drawing.Size(72, 72);
            this.picture_box_h.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picture_box_h.TabIndex = 18;
            this.picture_box_h.TabStop = false;
            // 
            // panel_output_name
            // 
            this.panel_output_name.Controls.Add(this.label_output_name);
            this.panel_output_name.Controls.Add(this.text_box_output_name);
            this.panel_output_name.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_output_name.Location = new System.Drawing.Point(0, 271);
            this.panel_output_name.Name = "panel_output_name";
            this.panel_output_name.Size = new System.Drawing.Size(896, 58);
            this.panel_output_name.TabIndex = 23;
            // 
            // label_output_name
            // 
            this.label_output_name.AutoSize = true;
            this.label_output_name.BackColor = System.Drawing.Color.Transparent;
            this.label_output_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_output_name.ForeColor = System.Drawing.Color.White;
            this.label_output_name.Location = new System.Drawing.Point(34, 3);
            this.label_output_name.Margin = new System.Windows.Forms.Padding(0);
            this.label_output_name.Name = "label_output_name";
            this.label_output_name.Size = new System.Drawing.Size(70, 13);
            this.label_output_name.TabIndex = 20;
            this.label_output_name.Text = "Output Name";
            // 
            // text_box_output_name
            // 
            this.text_box_output_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(63)))));
            this.text_box_output_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_box_output_name.ForeColor = System.Drawing.Color.White;
            this.text_box_output_name.Location = new System.Drawing.Point(36, 23);
            this.text_box_output_name.Name = "text_box_output_name";
            this.text_box_output_name.Size = new System.Drawing.Size(400, 20);
            this.text_box_output_name.TabIndex = 21;
            // 
            // panel_size_padding
            // 
            this.panel_size_padding.Controls.Add(this.panel_padding);
            this.panel_size_padding.Controls.Add(this.panel_size);
            this.panel_size_padding.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_size_padding.Location = new System.Drawing.Point(0, 213);
            this.panel_size_padding.Name = "panel_size_padding";
            this.panel_size_padding.Size = new System.Drawing.Size(896, 58);
            this.panel_size_padding.TabIndex = 26;
            // 
            // panel_padding
            // 
            this.panel_padding.Controls.Add(this.label_dip_2);
            this.panel_padding.Controls.Add(this.text_box_padding);
            this.panel_padding.Controls.Add(this.label_padding);
            this.panel_padding.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_padding.Location = new System.Drawing.Point(140, 0);
            this.panel_padding.Name = "panel_padding";
            this.panel_padding.Size = new System.Drawing.Size(140, 58);
            this.panel_padding.TabIndex = 24;
            // 
            // label_dip_2
            // 
            this.label_dip_2.AutoSize = true;
            this.label_dip_2.BackColor = System.Drawing.Color.Transparent;
            this.label_dip_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_dip_2.ForeColor = System.Drawing.Color.White;
            this.label_dip_2.Location = new System.Drawing.Point(119, 26);
            this.label_dip_2.Margin = new System.Windows.Forms.Padding(0);
            this.label_dip_2.Name = "label_dip_2";
            this.label_dip_2.Size = new System.Drawing.Size(21, 13);
            this.label_dip_2.TabIndex = 23;
            this.label_dip_2.Text = "dip";
            // 
            // text_box_padding
            // 
            this.text_box_padding.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(63)))));
            this.text_box_padding.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_box_padding.ForeColor = System.Drawing.Color.White;
            this.text_box_padding.Location = new System.Drawing.Point(36, 23);
            this.text_box_padding.Name = "text_box_padding";
            this.text_box_padding.Size = new System.Drawing.Size(80, 20);
            this.text_box_padding.TabIndex = 22;
            // 
            // label_padding
            // 
            this.label_padding.AutoSize = true;
            this.label_padding.BackColor = System.Drawing.Color.Transparent;
            this.label_padding.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_padding.ForeColor = System.Drawing.Color.White;
            this.label_padding.Location = new System.Drawing.Point(34, 3);
            this.label_padding.Margin = new System.Windows.Forms.Padding(0);
            this.label_padding.Name = "label_padding";
            this.label_padding.Size = new System.Drawing.Size(46, 13);
            this.label_padding.TabIndex = 22;
            this.label_padding.Text = "Padding";
            // 
            // panel_size
            // 
            this.panel_size.Controls.Add(this.label_dip_1);
            this.panel_size.Controls.Add(this.text_box_size);
            this.panel_size.Controls.Add(this.label_size);
            this.panel_size.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_size.Location = new System.Drawing.Point(0, 0);
            this.panel_size.Name = "panel_size";
            this.panel_size.Size = new System.Drawing.Size(140, 58);
            this.panel_size.TabIndex = 0;
            // 
            // label_dip_1
            // 
            this.label_dip_1.AutoSize = true;
            this.label_dip_1.BackColor = System.Drawing.Color.Transparent;
            this.label_dip_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_dip_1.ForeColor = System.Drawing.Color.White;
            this.label_dip_1.Location = new System.Drawing.Point(119, 26);
            this.label_dip_1.Margin = new System.Windows.Forms.Padding(0);
            this.label_dip_1.Name = "label_dip_1";
            this.label_dip_1.Size = new System.Drawing.Size(21, 13);
            this.label_dip_1.TabIndex = 23;
            this.label_dip_1.Text = "dip";
            // 
            // text_box_size
            // 
            this.text_box_size.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(63)))));
            this.text_box_size.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_box_size.ForeColor = System.Drawing.Color.White;
            this.text_box_size.Location = new System.Drawing.Point(36, 23);
            this.text_box_size.Name = "text_box_size";
            this.text_box_size.Size = new System.Drawing.Size(80, 20);
            this.text_box_size.TabIndex = 22;
            // 
            // label_size
            // 
            this.label_size.AutoSize = true;
            this.label_size.BackColor = System.Drawing.Color.Transparent;
            this.label_size.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_size.ForeColor = System.Drawing.Color.White;
            this.label_size.Location = new System.Drawing.Point(34, 3);
            this.label_size.Margin = new System.Windows.Forms.Padding(0);
            this.label_size.Name = "label_size";
            this.label_size.Size = new System.Drawing.Size(27, 13);
            this.label_size.TabIndex = 22;
            this.label_size.Text = "Size";
            // 
            // panel_source_attr
            // 
            this.panel_source_attr.Controls.Add(this.panel_source_color);
            this.panel_source_attr.Controls.Add(this.panel_source_padding);
            this.panel_source_attr.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_source_attr.Location = new System.Drawing.Point(0, 150);
            this.panel_source_attr.Name = "panel_source_attr";
            this.panel_source_attr.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.panel_source_attr.Size = new System.Drawing.Size(896, 63);
            this.panel_source_attr.TabIndex = 27;
            // 
            // panel_source_color
            // 
            this.panel_source_color.Controls.Add(this.label_source_color_opacity);
            this.panel_source_color.Controls.Add(this.label_source_opacity_value);
            this.panel_source_color.Controls.Add(this.trackbar_source_opacity);
            this.panel_source_color.Controls.Add(this.button_source_color_picker);
            this.panel_source_color.Controls.Add(this.label_source_color);
            this.panel_source_color.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_source_color.Location = new System.Drawing.Point(378, 5);
            this.panel_source_color.Name = "panel_source_color";
            this.panel_source_color.Size = new System.Drawing.Size(462, 58);
            this.panel_source_color.TabIndex = 25;
            // 
            // label_source_color_opacity
            // 
            this.label_source_color_opacity.AutoSize = true;
            this.label_source_color_opacity.BackColor = System.Drawing.Color.Transparent;
            this.label_source_color_opacity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_source_color_opacity.ForeColor = System.Drawing.Color.White;
            this.label_source_color_opacity.Location = new System.Drawing.Point(115, 3);
            this.label_source_color_opacity.Margin = new System.Windows.Forms.Padding(0);
            this.label_source_color_opacity.Name = "label_source_color_opacity";
            this.label_source_color_opacity.Size = new System.Drawing.Size(109, 13);
            this.label_source_color_opacity.TabIndex = 25;
            this.label_source_color_opacity.Text = "Overlay Color Opacity";
            // 
            // label_source_opacity_value
            // 
            this.label_source_opacity_value.AutoSize = true;
            this.label_source_opacity_value.BackColor = System.Drawing.Color.Transparent;
            this.label_source_opacity_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_source_opacity_value.ForeColor = System.Drawing.Color.White;
            this.label_source_opacity_value.Location = new System.Drawing.Point(420, 27);
            this.label_source_opacity_value.Margin = new System.Windows.Forms.Padding(0);
            this.label_source_opacity_value.Name = "label_source_opacity_value";
            this.label_source_opacity_value.Size = new System.Drawing.Size(21, 13);
            this.label_source_opacity_value.TabIndex = 24;
            this.label_source_opacity_value.Text = "0%";
            // 
            // trackbar_source_opacity
            // 
            this.trackbar_source_opacity.Enabled = false;
            this.trackbar_source_opacity.Location = new System.Drawing.Point(108, 23);
            this.trackbar_source_opacity.Maximum = 100;
            this.trackbar_source_opacity.Name = "trackbar_source_opacity";
            this.trackbar_source_opacity.Size = new System.Drawing.Size(308, 45);
            this.trackbar_source_opacity.TabIndex = 0;
            this.trackbar_source_opacity.TabStop = false;
            this.trackbar_source_opacity.TickFrequency = 5;
            this.trackbar_source_opacity.ValueChanged += new System.EventHandler(this.trackbar_source_opacity_ValueChanged);
            this.trackbar_source_opacity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.trackbar_source_opacity_KeyDown);
            this.trackbar_source_opacity.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trackbar_source_opacity_MouseUp);
            this.trackbar_source_opacity.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.trackbar_source_opacity_MouseWheel);
            // 
            // button_source_color_picker
            // 
            this.button_source_color_picker.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(87)))), ((int)(((byte)(69)))));
            this.button_source_color_picker.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_source_color_picker.Enabled = false;
            this.button_source_color_picker.FlatAppearance.BorderSize = 0;
            this.button_source_color_picker.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.button_source_color_picker.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.button_source_color_picker.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_source_color_picker.ForeColor = System.Drawing.Color.White;
            this.button_source_color_picker.Location = new System.Drawing.Point(37, 23);
            this.button_source_color_picker.Name = "button_source_color_picker";
            this.button_source_color_picker.Size = new System.Drawing.Size(20, 20);
            this.button_source_color_picker.TabIndex = 15;
            this.button_source_color_picker.UseVisualStyleBackColor = false;
            this.button_source_color_picker.Click += new System.EventHandler(this.button_color_picker_Click);
            // 
            // label_source_color
            // 
            this.label_source_color.AutoSize = true;
            this.label_source_color.BackColor = System.Drawing.Color.Transparent;
            this.label_source_color.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_source_color.ForeColor = System.Drawing.Color.White;
            this.label_source_color.Location = new System.Drawing.Point(34, 3);
            this.label_source_color.Margin = new System.Windows.Forms.Padding(0);
            this.label_source_color.Name = "label_source_color";
            this.label_source_color.Size = new System.Drawing.Size(70, 13);
            this.label_source_color.TabIndex = 22;
            this.label_source_color.Text = "Overlay Color";
            // 
            // panel_source_padding
            // 
            this.panel_source_padding.Controls.Add(this.label_source_padding_value);
            this.panel_source_padding.Controls.Add(this.trackbar_source_padding);
            this.panel_source_padding.Controls.Add(this.label_source_padding);
            this.panel_source_padding.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_source_padding.Location = new System.Drawing.Point(0, 5);
            this.panel_source_padding.Name = "panel_source_padding";
            this.panel_source_padding.Size = new System.Drawing.Size(378, 58);
            this.panel_source_padding.TabIndex = 25;
            // 
            // label_source_padding_value
            // 
            this.label_source_padding_value.AutoSize = true;
            this.label_source_padding_value.BackColor = System.Drawing.Color.Transparent;
            this.label_source_padding_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_source_padding_value.ForeColor = System.Drawing.Color.White;
            this.label_source_padding_value.Location = new System.Drawing.Point(340, 27);
            this.label_source_padding_value.Margin = new System.Windows.Forms.Padding(0);
            this.label_source_padding_value.Name = "label_source_padding_value";
            this.label_source_padding_value.Size = new System.Drawing.Size(21, 13);
            this.label_source_padding_value.TabIndex = 24;
            this.label_source_padding_value.Text = "0%";
            // 
            // trackbar_source_padding
            // 
            this.trackbar_source_padding.Enabled = false;
            this.trackbar_source_padding.Location = new System.Drawing.Point(29, 23);
            this.trackbar_source_padding.Maximum = 50;
            this.trackbar_source_padding.Minimum = -50;
            this.trackbar_source_padding.Name = "trackbar_source_padding";
            this.trackbar_source_padding.Size = new System.Drawing.Size(308, 45);
            this.trackbar_source_padding.TabIndex = 0;
            this.trackbar_source_padding.TabStop = false;
            this.trackbar_source_padding.TickFrequency = 5;
            this.trackbar_source_padding.ValueChanged += new System.EventHandler(this.trackbar_source_padding_ValueChanged);
            this.trackbar_source_padding.KeyDown += new System.Windows.Forms.KeyEventHandler(this.trackbar_source_padding_KeyDown);
            this.trackbar_source_padding.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trackbar_source_padding_MouseUp);
            this.trackbar_source_padding.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.trackbar_source_padding_MouseWheel);
            // 
            // label_source_padding
            // 
            this.label_source_padding.AutoSize = true;
            this.label_source_padding.BackColor = System.Drawing.Color.Transparent;
            this.label_source_padding.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_source_padding.ForeColor = System.Drawing.Color.White;
            this.label_source_padding.Location = new System.Drawing.Point(34, 3);
            this.label_source_padding.Margin = new System.Windows.Forms.Padding(0);
            this.label_source_padding.Name = "label_source_padding";
            this.label_source_padding.Size = new System.Drawing.Size(83, 13);
            this.label_source_padding.TabIndex = 22;
            this.label_source_padding.Text = "Source Padding";
            // 
            // panel_bottom
            // 
            this.panel_bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.panel_bottom.Controls.Add(this.text_box_status);
            this.panel_bottom.Controls.Add(this.button_home);
            this.panel_bottom.Controls.Add(this.button_generate);
            this.panel_bottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_bottom.Location = new System.Drawing.Point(0, 575);
            this.panel_bottom.Name = "panel_bottom";
            this.panel_bottom.Size = new System.Drawing.Size(896, 75);
            this.panel_bottom.TabIndex = 25;
            // 
            // text_box_status
            // 
            this.text_box_status.AutoSize = true;
            this.text_box_status.BackColor = System.Drawing.Color.Transparent;
            this.text_box_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_box_status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.text_box_status.Location = new System.Drawing.Point(34, 19);
            this.text_box_status.Margin = new System.Windows.Forms.Padding(0);
            this.text_box_status.MaximumSize = new System.Drawing.Size(500, 37);
            this.text_box_status.MinimumSize = new System.Drawing.Size(500, 37);
            this.text_box_status.Name = "text_box_status";
            this.text_box_status.Size = new System.Drawing.Size(500, 37);
            this.text_box_status.TabIndex = 15;
            // 
            // button_home
            // 
            this.button_home.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_home.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.button_home.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_home.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(73)))), ((int)(((byte)(74)))));
            this.button_home.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.button_home.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.button_home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_home.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_home.ForeColor = System.Drawing.Color.White;
            this.button_home.Location = new System.Drawing.Point(700, 21);
            this.button_home.Name = "button_home";
            this.button_home.Size = new System.Drawing.Size(75, 35);
            this.button_home.TabIndex = 14;
            this.button_home.Text = "Go Back";
            this.button_home.UseVisualStyleBackColor = false;
            this.button_home.Click += new System.EventHandler(this.button_home_Click);
            // 
            // button_generate
            // 
            this.button_generate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_generate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.button_generate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_generate.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(73)))), ((int)(((byte)(74)))));
            this.button_generate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.button_generate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.button_generate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_generate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_generate.ForeColor = System.Drawing.Color.White;
            this.button_generate.Location = new System.Drawing.Point(794, 13);
            this.button_generate.Name = "button_generate";
            this.button_generate.Size = new System.Drawing.Size(90, 50);
            this.button_generate.TabIndex = 13;
            this.button_generate.Text = "Generate";
            this.button_generate.UseVisualStyleBackColor = false;
            this.button_generate.Click += new System.EventHandler(this.button_generate_Click);
            // 
            // panel_dir
            // 
            this.panel_dir.Controls.Add(this.label_source);
            this.panel_dir.Controls.Add(this.button_source);
            this.panel_dir.Controls.Add(this.button_destination);
            this.panel_dir.Controls.Add(this.text_box_source);
            this.panel_dir.Controls.Add(this.label_destination);
            this.panel_dir.Controls.Add(this.text_box_destination);
            this.panel_dir.Controls.Add(this.picture_box_main);
            this.panel_dir.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_dir.Location = new System.Drawing.Point(0, 0);
            this.panel_dir.Name = "panel_dir";
            this.panel_dir.Size = new System.Drawing.Size(896, 150);
            this.panel_dir.TabIndex = 22;
            // 
            // label_source
            // 
            this.label_source.AutoSize = true;
            this.label_source.BackColor = System.Drawing.Color.Transparent;
            this.label_source.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_source.ForeColor = System.Drawing.Color.White;
            this.label_source.Location = new System.Drawing.Point(34, 28);
            this.label_source.Margin = new System.Windows.Forms.Padding(0);
            this.label_source.Name = "label_source";
            this.label_source.Size = new System.Drawing.Size(73, 13);
            this.label_source.TabIndex = 9;
            this.label_source.Text = "Source Image";
            // 
            // button_source
            // 
            this.button_source.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.button_source.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_source.FlatAppearance.BorderSize = 0;
            this.button_source.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.button_source.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.button_source.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_source.ForeColor = System.Drawing.Color.White;
            this.button_source.Location = new System.Drawing.Point(442, 48);
            this.button_source.Name = "button_source";
            this.button_source.Size = new System.Drawing.Size(75, 23);
            this.button_source.TabIndex = 8;
            this.button_source.Text = "Browse";
            this.button_source.UseVisualStyleBackColor = false;
            this.button_source.Click += new System.EventHandler(this.button_source_Click);
            // 
            // button_destination
            // 
            this.button_destination.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.button_destination.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_destination.FlatAppearance.BorderSize = 0;
            this.button_destination.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.button_destination.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.button_destination.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_destination.ForeColor = System.Drawing.Color.White;
            this.button_destination.Location = new System.Drawing.Point(442, 112);
            this.button_destination.Name = "button_destination";
            this.button_destination.Size = new System.Drawing.Size(75, 23);
            this.button_destination.TabIndex = 10;
            this.button_destination.Text = "Browse";
            this.button_destination.UseVisualStyleBackColor = false;
            this.button_destination.Click += new System.EventHandler(this.button_destination_Click);
            // 
            // text_box_source
            // 
            this.text_box_source.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(63)))));
            this.text_box_source.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_box_source.ForeColor = System.Drawing.Color.White;
            this.text_box_source.Location = new System.Drawing.Point(36, 48);
            this.text_box_source.Name = "text_box_source";
            this.text_box_source.ReadOnly = true;
            this.text_box_source.Size = new System.Drawing.Size(400, 20);
            this.text_box_source.TabIndex = 11;
            this.text_box_source.TextChanged += new System.EventHandler(this.text_box_source_TextChanged);
            // 
            // label_destination
            // 
            this.label_destination.AutoSize = true;
            this.label_destination.BackColor = System.Drawing.Color.Transparent;
            this.label_destination.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_destination.ForeColor = System.Drawing.Color.White;
            this.label_destination.Location = new System.Drawing.Point(34, 92);
            this.label_destination.Margin = new System.Windows.Forms.Padding(0);
            this.label_destination.Name = "label_destination";
            this.label_destination.Size = new System.Drawing.Size(92, 13);
            this.label_destination.TabIndex = 12;
            this.label_destination.Text = "Destination Folder";
            // 
            // text_box_destination
            // 
            this.text_box_destination.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(63)))));
            this.text_box_destination.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_box_destination.ForeColor = System.Drawing.Color.White;
            this.text_box_destination.Location = new System.Drawing.Point(36, 112);
            this.text_box_destination.Name = "text_box_destination";
            this.text_box_destination.ReadOnly = true;
            this.text_box_destination.Size = new System.Drawing.Size(400, 20);
            this.text_box_destination.TabIndex = 13;
            this.text_box_destination.TextChanged += new System.EventHandler(this.text_box_destination_TextChanged);
            // 
            // picture_box_main
            // 
            this.picture_box_main.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(58)))));
            this.picture_box_main.Location = new System.Drawing.Point(586, 28);
            this.picture_box_main.Name = "picture_box_main";
            this.picture_box_main.Size = new System.Drawing.Size(107, 107);
            this.picture_box_main.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picture_box_main.TabIndex = 14;
            this.picture_box_main.TabStop = false;
            // 
            // button_action_bar_and_tab_icons
            // 
            this.button_action_bar_and_tab_icons.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_action_bar_and_tab_icons.AutoSize = true;
            this.button_action_bar_and_tab_icons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.button_action_bar_and_tab_icons.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_action_bar_and_tab_icons.FlatAppearance.BorderSize = 0;
            this.button_action_bar_and_tab_icons.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.button_action_bar_and_tab_icons.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.button_action_bar_and_tab_icons.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_action_bar_and_tab_icons.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_action_bar_and_tab_icons.ForeColor = System.Drawing.Color.White;
            this.button_action_bar_and_tab_icons.Location = new System.Drawing.Point(345, 209);
            this.button_action_bar_and_tab_icons.Name = "button_action_bar_and_tab_icons";
            this.button_action_bar_and_tab_icons.Size = new System.Drawing.Size(206, 30);
            this.button_action_bar_and_tab_icons.TabIndex = 3;
            this.button_action_bar_and_tab_icons.Text = "Action Bar and Tab Icons";
            this.button_action_bar_and_tab_icons.UseVisualStyleBackColor = false;
            // 
            // button_launcher_icons
            // 
            this.button_launcher_icons.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_launcher_icons.AutoSize = true;
            this.button_launcher_icons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.button_launcher_icons.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_launcher_icons.FlatAppearance.BorderSize = 0;
            this.button_launcher_icons.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.button_launcher_icons.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.button_launcher_icons.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_launcher_icons.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_launcher_icons.ForeColor = System.Drawing.Color.White;
            this.button_launcher_icons.Location = new System.Drawing.Point(377, 173);
            this.button_launcher_icons.Name = "button_launcher_icons";
            this.button_launcher_icons.Size = new System.Drawing.Size(143, 30);
            this.button_launcher_icons.TabIndex = 2;
            this.button_launcher_icons.Text = "Launcher Icons";
            this.button_launcher_icons.UseVisualStyleBackColor = false;
            this.button_launcher_icons.Click += new System.EventHandler(this.button_launcher_icons_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.ClientSize = new System.Drawing.Size(896, 650);
            this.Controls.Add(this.panel_main);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(816, 600);
            this.Name = "MainForm";
            this.Text = "Zeus - Android Asset Studio";
            this.panel_main.ResumeLayout(false);
            this.panel_main.PerformLayout();
            this.panel_submain.ResumeLayout(false);
            this.panel_image_output_normal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_xxx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_xx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_x)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_m)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_h)).EndInit();
            this.panel_output_name.ResumeLayout(false);
            this.panel_output_name.PerformLayout();
            this.panel_size_padding.ResumeLayout(false);
            this.panel_padding.ResumeLayout(false);
            this.panel_padding.PerformLayout();
            this.panel_size.ResumeLayout(false);
            this.panel_size.PerformLayout();
            this.panel_source_attr.ResumeLayout(false);
            this.panel_source_color.ResumeLayout(false);
            this.panel_source_color.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackbar_source_opacity)).EndInit();
            this.panel_source_padding.ResumeLayout(false);
            this.panel_source_padding.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackbar_source_padding)).EndInit();
            this.panel_bottom.ResumeLayout(false);
            this.panel_bottom.PerformLayout();
            this.panel_dir.ResumeLayout(false);
            this.panel_dir.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_main)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void Trackbar_source_padding_PreviewKeyDown(object sender, System.Windows.Forms.PreviewKeyDownEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Panel panel_main;
        private System.Windows.Forms.Button button_launcher_icons;
        private System.Windows.Forms.Button button_action_bar_and_tab_icons;

        private System.Windows.Forms.Panel panel_submain;
        private System.Windows.Forms.Label label_source;
        private System.Windows.Forms.TextBox text_box_source;
        private System.Windows.Forms.Label label_destination;
        private System.Windows.Forms.TextBox text_box_destination;
        private System.Windows.Forms.Button button_source;
        private System.Windows.Forms.Button button_destination;
        private System.Windows.Forms.PictureBox picture_box_main;
        private System.Windows.Forms.PictureBox picture_box_m;
        private System.Windows.Forms.PictureBox picture_box_h;
        private System.Windows.Forms.PictureBox picture_box_x;
        private System.Windows.Forms.PictureBox picture_box_xx;
        private System.Windows.Forms.PictureBox picture_box_xxx;
        private System.Windows.Forms.TextBox text_box_output_name;
        private System.Windows.Forms.Label label_output_name;
        private System.Windows.Forms.Panel panel_dir;
        private System.Windows.Forms.Panel panel_output_name;
        private System.Windows.Forms.Panel panel_image_output_normal;
        private System.Windows.Forms.Panel panel_bottom;
        private System.Windows.Forms.Button button_generate;
        private System.Windows.Forms.Panel panel_size_padding;
        private System.Windows.Forms.Panel panel_size;
        private System.Windows.Forms.Label label_size;
        private System.Windows.Forms.TextBox text_box_size;
        private System.Windows.Forms.Label label_dip_1;
        private System.Windows.Forms.Panel panel_source_color;
        private System.Windows.Forms.Label label_source_color;
        private System.Windows.Forms.Panel panel_padding;
        private System.Windows.Forms.Label label_dip_2;
        private System.Windows.Forms.TextBox text_box_padding;
        private System.Windows.Forms.Label label_padding;
        private System.Windows.Forms.Button button_source_color_picker;
        private System.Windows.Forms.TrackBar trackbar_source_opacity;
        private System.Windows.Forms.Label label_source_opacity_value;
        private System.Windows.Forms.Button button_home;
        private System.Windows.Forms.Label text_box_status;
        private System.Windows.Forms.Panel panel_source_attr;
        private System.Windows.Forms.Panel panel_source_padding;
        private System.Windows.Forms.Label label_source_padding_value;
        private System.Windows.Forms.TrackBar trackbar_source_padding;
        private System.Windows.Forms.Label label_source_padding;
        private System.Windows.Forms.Label label_source_color_opacity;
    }
}